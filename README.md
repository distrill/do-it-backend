## do-it

This is a backend for a productivity tracking application. It allows users to add things they want to be practicing or doing every (most) days, and to weight those things (perhaps studying statistics every day is more important than exercise to some, or vice versa). Then users can check in every day to see over time how they have been improving.
